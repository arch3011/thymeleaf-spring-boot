package com.example.Hanuman.repository;

import com.example.Hanuman.model.Photu;
import com.example.Hanuman.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhotuRepository extends JpaRepository<Photu, String>
{

}
