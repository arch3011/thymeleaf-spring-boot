package com.example.Hanuman.repository;

import com.example.Hanuman.model.Photu;
import com.example.Hanuman.model.Rishabh;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RishabhRepository extends JpaRepository<Rishabh, Integer>
{

}
