package com.example.Hanuman.controller;
//
//import com.example.Hanuman.model.Photu;
//import com.example.Hanuman.model.Product;
//import com.example.Hanuman.service.PhotuService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//@Controller
//public class PhotuController
//{
//
//    @Autowired
//    private PhotuService photuService;
//
//    @RequestMapping(value = "/photoSave" , method = RequestMethod.POST)
//    public String saveProduct(@ModelAttribute("photu") Photu photu)
//    {
//        photuService.save(photu);
//        return "redirect:/";
//    }
//
//
//}
//
//
//
//
//package com.example.filedemo.controller;

        import com.example.Hanuman.model.Photu;
        import com.example.Hanuman.model.Product;
        import com.example.Hanuman.service.PhotuService;
        import com.example.Hanuman.payload.UploadFileResponse;
        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.core.io.ByteArrayResource;
        import org.springframework.core.io.Resource;
        import org.springframework.http.HttpHeaders;
        import org.springframework.http.MediaType;
        import org.springframework.http.ResponseEntity;
        import org.springframework.stereotype.Controller;
        import org.springframework.ui.Model;
        import org.springframework.web.bind.annotation.*;
        import org.springframework.web.multipart.MultipartFile;
        import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

        import java.util.Arrays;
        import java.util.Base64;
        import java.util.List;
        import java.util.stream.Collectors;

@Controller
public class PhotuController {

    private static final Logger logger = LoggerFactory.getLogger(PhotuController.class);

    @Autowired
    private PhotuService photuService;

    @RequestMapping("/photos/{id}")
    public String getPhoto(@PathVariable String id, Model model) {
        Photu photu = photuService.getPhoto(id);
        model.addAttribute("id", photu.getId());
        model.addAttribute("image",
                Base64.getEncoder().encodeToString(photu.getData().clone()));
        return "displayPage";
    }




    @PostMapping("/uploadFile")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        Photu photu = photuService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(photu.getId())
                .toUriString();

        return new UploadFileResponse(photu.getFileName(), fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    @RequestMapping("/rishabh")
    public String showNewPhotuForm(Model model)
    {
        Photu photu = new Photu();
        model.addAttribute("photu" , photu);
        return "uploadPage";
    }

    @PostMapping("/uploadFileTry")
    public String uploadFileTry(@RequestParam("file") MultipartFile file)
    {
        Photu photu = photuService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(photu.getId())
                .toUriString();
        return "index";
    }


    @PostMapping("/uploadMultipleFiles")
    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
                .stream()
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
    }

    @GetMapping("/downloadFile/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileId) {
        // Load file from database
        Photu photu = photuService.getFile(fileId);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(photu.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + photu.getFileName() + "\"")
                .body(new ByteArrayResource(photu.getData()));
    }

}