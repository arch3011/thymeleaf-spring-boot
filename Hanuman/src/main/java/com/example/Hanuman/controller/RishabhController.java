package com.example.Hanuman.controller;

import com.example.Hanuman.model.Photu;
import com.example.Hanuman.model.Rishabh;
import com.example.Hanuman.repository.RishabhRepository;
import com.example.Hanuman.service.PhotuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.Optional;

@Controller
public class RishabhController
{
    @Autowired
    private RishabhRepository rishabhRepository;

    @GetMapping("database/{id}")
    public ResponseEntity<byte[]> fromDatabaseAsResEntity(@PathVariable("id") Integer id) throws SQLException {

        Optional<Rishabh> primeMinisterOfIndia = rishabhRepository.findById(id);
        byte[] imageBytes = null;
        if (primeMinisterOfIndia.isPresent()) {

            imageBytes = primeMinisterOfIndia.get().getPhoto().getBytes(1,
                    (int) primeMinisterOfIndia.get().getPhoto().length());
        }

        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(imageBytes);
    }

}
