package com.example.Hanuman.controller;

import com.example.Hanuman.model.Product;
import com.example.Hanuman.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


import java.util.List;

@Controller
public class AppController
{
    @Autowired
    private ProductService service;

    @RequestMapping("/")
    public String viewHomePgae(Model model)
    {
        List <Product> listProducts = service.listAll();
        model.addAttribute("listProducts" , listProducts);

        return "index";
    }

    @RequestMapping("/new")
    public String showNewProductForm(Model model)
    {
        Product product = new Product();
        model.addAttribute("product" , product);
        return "new_product";
    }

    @RequestMapping("/old")
    public String searchProductForm(Model model)
    {
        Product product = new Product();
        model.addAttribute("product" , product);
        return "searchById";
    }

    @RequestMapping("/med")
    public String searchProductFormForDelete(Model model)
    {
        Product product = new Product();
        model.addAttribute("product" , product);
        return "searchByIdToDelete";
    }

    @RequestMapping(value = "/save" , method = RequestMethod.POST)
    public String saveProduct(@ModelAttribute("product") Product product)
    {
        service.save(product);
        return "redirect:/";
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView showEditProductForm(@PathVariable(name = "id") Long id)
    {
        ModelAndView mav = new ModelAndView("edit_product");
        Product product = service.get(id);
        mav.addObject("product" , product);
        return mav;
    }

        @RequestMapping(value = "/directed" , method = RequestMethod.POST)
    public String directCreate(@ModelAttribute("product") Model model)
    {
        Product product = new Product();
        model.addAttribute("product" , product);
        service.save(product);
        return "redirect:/";
    }

    @RequestMapping("/deleteBasic/{id}")
    public String deleteProduct(@PathVariable(name = "id") Long id)
    {
    service.delete(id);
    return "redirect:/";
    }
    @RequestMapping("/search")
    public String del(@PathVariable(name = "id") Long id)
    {
        return "redirect:/";
    }


    //_______________


//    @RequestMapping(value = "students/{surname}", method = RequestMethod.GET)
//    public String showStudentBySurname(@PathVariable String surname, Model model) {
//        model.addAttribute("search", studentService.listStudentsBySurname(surname));
//        return "students";
//    }



//    @RequestMapping("/search/{id}")
//    public ModelAndView updateById(@PathVariable(name = "id") Long id)
//    {
////        service.get(id);
////        return "edit_product";
//        ModelAndView mav = new ModelAndView("edit_product");
//        Product product = service.get(id);
//        mav.addObject("product" , product);
//        return mav;
//    }
//

    @GetMapping("/search/{id}")
    public ModelAndView updateById(@RequestParam(value = "id", required = false) Long id , Model model)
    {
//        service.get(id);
//        return "edit_product";
        ModelAndView mav = new ModelAndView("edit_product");
        Product product = service.get(id);
        mav.addObject("product" , product);
        return mav;
    }

    @RequestMapping("/delete/{id}")
    public ModelAndView deleteById(@RequestParam(value = "id", required = false) Long id , Model model)
    {
//        service.get(id);
//        return "edit_product";
        ModelAndView mav = new ModelAndView("delete_product");
        Product product = service.get(id);
        mav.addObject("product" , product);
        return mav;
    }

//    @GetMapping("/search/{id}")
//    public String showStudentBySurname(@PathVariable(name = "id") Long id , Model model)
//    {
//        model.addAttribute("product", service.get(id));
//        return "edit_product";
//    }


//    @RequestMapping("/update/{id}")
//public ModelAndView showUpdateProductForm(@PathVariable(name = "id") Long id)
//{
//    ModelAndView mav = new ModelAndView("edit_product");
//    Product product = service.get(id);
//    mav.addObject("product" , product);
//    return mav;
//}
}
