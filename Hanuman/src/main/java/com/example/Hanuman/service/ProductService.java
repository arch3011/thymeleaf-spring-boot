package com.example.Hanuman.service;

import com.example.Hanuman.model.Product;
import com.example.Hanuman.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService
{
    @Autowired
    private ProductRepository repo;

    public List<Product> listAll()
    {
        return repo.findAll();
    }

    public HttpStatus save(Product product)
    {
        repo.save(product);
        return HttpStatus.OK;
    }

    public Product get(Long id)
    {
        return repo.findById(id).get(); //find by id method give in form of object thats why get method
    }

    public void delete(Long id)
    {
        repo.deleteById(id);
    }

}
