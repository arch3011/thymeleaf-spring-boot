package com.example.Hanuman.service;


import com.example.Hanuman.exception.FileStorageException;
import com.example.Hanuman.exception.MyFileNotFoundException;
import com.example.Hanuman.model.Photu;
import com.example.Hanuman.model.Rishabh;
import com.example.Hanuman.repository.PhotuRepository;
import com.example.Hanuman.repository.RishabhRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

@Service
public class PhotuService
{

    @Autowired
    private PhotuRepository photuRepository;

    @Autowired
    private RishabhRepository rishabhRepository;


    public Photu storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            Photu photu = new Photu(fileName, file.getContentType(), file.getBytes());

            return photuRepository.save(photu);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Photu getFile(String fileId) {
        return photuRepository.findById(fileId)
                .orElseThrow(() -> new MyFileNotFoundException("File not found with id " + fileId));
    }

    public Photu getPhoto(String id) {
        return photuRepository.findById(id).get();
    }


    @GetMapping("database/{id}")
    public ResponseEntity<byte[]> fromDatabaseAsResEntity(@PathVariable("id") Integer id)
            throws SQLException {

        Optional<Rishabh> rishi = rishabhRepository.findById(id);
        byte[] imageBytes = null;
        if (rishi.isPresent()) {

            imageBytes = rishi.get().getPhoto().getBytes(1,
                    (int) rishi.get().getPhoto().length());
        }

        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(imageBytes);
    }

}