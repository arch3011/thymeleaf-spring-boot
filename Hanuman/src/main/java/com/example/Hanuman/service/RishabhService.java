//package com.example.Hanuman.service;
//
//import com.example.Hanuman.exception.FileStorageException;
//import com.example.Hanuman.model.Photu;
//import com.example.Hanuman.model.Rishabh;
//import com.example.Hanuman.repository.RishabhRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.util.StringUtils;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.websocket.Encoder;
//import java.io.IOException;
//
//@Service
//public class RishabhService {
//
//    @Autowired
//    private RishabhRepository photoRepo;
//
//    public String addPhoto(String title, MultipartFile file) throws IOException {
//        Rishabh photo = new Rishabh(title);
//        photo.setImage(
//                new Encoder.Binary(BsonBinarySubType.BINARY, file.getBytes()));
//        photo = photoRepo.insert(photo); return photo.getId();
//    }
////
////    public Rishabh storeFile(MultipartFile file) {
//         Normalize file name
////        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
////
////        try {
//             Check if the file's name contains invalid characters
////            if(fileName.contains("..")) {
//                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
//            }
//
//            Rishabh photu = new Rishabh(fileName, file.getContentType(), file.getBinary());
//
//            return photoRepo.save(file);
//        } catch (IOException ex) {
//            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
//        }
//    }
//
//
//    public Rishabh getPhoto(String id) {
//        return photoRepo.findById(id).get();
//    }
//}
//