package com.example.Hanuman.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.websocket.Encoder;
import java.io.InputStream;
import java.sql.Blob;

@Entity
public class Rishabh
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Blob photo;

    public Rishabh() {
    }

    public Rishabh(Integer id, Blob photo) {
        this.id = id;
        this.photo = photo;
    }

    public Rishabh(Blob photo) {
        this.photo = photo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Blob getPhoto() {
        return photo;
    }

    public void setPhoto(Blob photo) {
        this.photo = photo;
    }

    //
//    @Id
//    private String id;
//
//    private String title;
//
////    private Encoder.Binary image;
//
//    public Rishabh() {
//    }
//
//    public Rishabh(String id, String title, Encoder.Binary image) {
//        this.id = id;
//        this.title = title;
////        this.image = image;
//    }
//
//    public Rishabh(String id) {
//        this.id = id;
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
////    public Encoder.Binary getImage() {
////        return image;
////    }
////
////    public void setImage(Encoder.Binary image) {
////        this.image = image;
////    }
}
