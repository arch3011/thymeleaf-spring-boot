package com.example.Hanuman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SankatMochanApplication {

	public static void main(String[] args)
	{
		SpringApplication.run(SankatMochanApplication.class, args);
	}

}